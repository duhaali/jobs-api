# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

20.times {|i| Job.create!({"id" => i+5, "status" =>"not seen", "job_detals" => "about job", "position" =>"sw", "company_name" => "company #{i + 1}"}) }