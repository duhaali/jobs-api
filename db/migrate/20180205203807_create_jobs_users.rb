class CreateJobsUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :jobs_users do |t|
      t.text :company_name
      t.text :position
      t.text :status
      t.text :job_detals

      t.timestamps
    end
  end
end
