Rails.application.routes.draw do
  root "jobs#index"
  resources :jobs
  resources :users, only: :create do
    collection do
      post 'confirm'
      post 'login'
    end
  end
  resources :sessions, only: [:update]
end
