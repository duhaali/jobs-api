class JobsUsersController < ApplicationController
  before_action :set_jobs_user, only: [:show, :edit, :update, :destroy]

  # GET /jobs_users
  # GET /jobs_users.json
  def index
    @jobs_users = JobsUser.all
  end

  # GET /jobs_users/1
  # GET /jobs_users/1.json
  def show
  end

  # GET /jobs_users/new
  def new
    @jobs_user = JobsUser.new
  end

  # GET /jobs_users/1/edit
  def edit
  end

  # POST /jobs_users
  # POST /jobs_users.json
  def create
    @jobs_user = JobsUser.new(jobs_user_params)

    respond_to do |format|
      if @jobs_user.save
        format.html { redirect_to @jobs_user, notice: 'Jobs user was successfully created.' }
        format.json { render :show, status: :created, location: @jobs_user }
      else
        format.html { render :new }
        format.json { render json: @jobs_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /jobs_users/1
  # PATCH/PUT /jobs_users/1.json
  def update
    respond_to do |format|
      if @jobs_user.update(jobs_user_params)
        format.html { redirect_to @jobs_user, notice: 'Jobs user was successfully updated.' }
        format.json { render :show, status: :ok, location: @jobs_user }
      else
        format.html { render :edit }
        format.json { render json: @jobs_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs_users/1
  # DELETE /jobs_users/1.json
  def destroy
    @jobs_user.destroy
    respond_to do |format|
      format.html { redirect_to jobs_users_url, notice: 'Jobs user was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_jobs_user
      @jobs_user = JobsUser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def jobs_user_params
      params.require(:jobs_user).permit(:company_name, :position, :status, :job_detals)
    end
end
