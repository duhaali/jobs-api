class JobsController < ApplicationController
  before_action :set_job, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  # GET /jobs
  # GET /jobs.json
  
  # GET /jobs/1
  # GET /jobs/1.json
  def show
  end

  # GET /jobs/new
  

  # GET /jobs/1/edit
  

  # POST /jobs
  # POST /jobs.json
  def create
    if @job.save
      flash[:success] = 'job was saved!'
      redirect_to root_path
    else
      render 'new'
    end
  end

  # PATCH/PUT /jobs/1
  # PATCH/PUT /jobs/1.json
  def update
    if @job.update_attributes(job_params)
      flash[:success] = 'job was updated!'
      redirect_to root_path
    else
      render 'edit'
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    if @job.destroy
      flash[:success] = 'job was destroyed!'
    else
      flash[:warning] = 'job destroy this job...'
    end
    redirect_to root_path
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Job.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_params
      params.require(:job).permit(:company_name, :position, :status, :job_detals)
    end
end