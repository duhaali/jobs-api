json.extract! jobs_user, :id, :company_name, :position, :status, :job_detals, :created_at, :updated_at
json.url jobs_user_url(jobs_user, format: :json)
