json.extract! job, :id, :company_name, :position, :status, :job_detals, :created_at, :updated_at
json.url job_url(job, format: :json)
